<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_product_title</name>
   <tag></tag>
   <elementGuidId>c6e3a932-7be7-4474-b084-6081eb12a9c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h5[@class='card-title fs-5 fw-bolder']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h5[@class='card-title fs-5 fw-bolder']</value>
      <webElementGuid>8929e541-6464-48d1-be69-49fdaa987773</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>b48be53c-9c93-42fd-82c1-bf618d3636f6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
