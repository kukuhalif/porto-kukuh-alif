<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_product_category</name>
   <tag></tag>
   <elementGuidId>6047d229-f7a2-4b87-b781-5a83e0640609</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='card p-2 rounded-4 shadow border-0']//div[@class='card-body']//p[@class='card-text text-black-50 fs-6']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='card p-2 rounded-4 shadow border-0']//div[@class='card-body']//p[@class='card-text text-black-50 fs-6']</value>
      <webElementGuid>589b77c1-9306-4fb9-b75c-49a831ded539</webElementGuid>
   </webElementProperties>
</WebElementEntity>
