<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_description</name>
   <tag></tag>
   <elementGuidId>a9340b5b-5760-49db-beac-8922bd96471c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h5[text()='Deskripsi']//following-sibling::p[@class='card-text text-black-50 fs-6']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h5[text()='Deskripsi']//following-sibling::p[@class='card-text text-black-50 fs-6']</value>
      <webElementGuid>57c7a0aa-755b-457a-8fbe-9ada6bee1f99</webElementGuid>
   </webElementProperties>
</WebElementEntity>
