<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_category</name>
   <tag></tag>
   <elementGuidId>fe5f1171-9b66-4fdb-9256-7c1484f901f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.p-2.rounded-4.shadow.border-0 > div.card-body > p.card-text.text-black-50.fs-6</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Incredible Rubber Shoes'])[1]/following::p[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>4d7e92bb-056f-4ee1-8c02-434d9fcea44b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-text text-black-50 fs-6</value>
      <webElementGuid>b88930c8-ef33-4f7d-9b64-537dccf94c72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kendaraan</value>
      <webElementGuid>46509dd5-5b6b-4ca8-8636-cc81cd8044bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card p-2 rounded-4 shadow border-0&quot;]/div[@class=&quot;card-body&quot;]/p[@class=&quot;card-text text-black-50 fs-6&quot;]</value>
      <webElementGuid>15f821c3-d368-4118-837d-3c3a30354fcc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Incredible Rubber Shoes'])[1]/following::p[1]</value>
      <webElementGuid>db8b6e2b-1231-49fa-b32c-47134036b05e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::p[2]</value>
      <webElementGuid>1dfa90ab-580d-476c-b38a-e61368514617</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya tertarik dan ingin nego'])[1]/preceding::p[2]</value>
      <webElementGuid>c7a38d31-a09a-4b22-ae6b-3427eb293c8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sonja Dach'])[1]/preceding::p[2]</value>
      <webElementGuid>815cb415-f057-4296-a7cb-2f29d8f82722</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kendaraan']/parent::*</value>
      <webElementGuid>d40826c5-1b53-478c-af9b-713679579d8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/p</value>
      <webElementGuid>7e0b1339-6c68-4d47-86be-582c004d3138</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Kendaraan' or . = 'Kendaraan')]</value>
      <webElementGuid>d9296609-e25f-4824-a9e9-314e70b24dd2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
