<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Hobi</name>
   <tag></tag>
   <elementGuidId>7d69d354-e445-4a67-ba6c-b0c0339494cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-alt-primary col rounded-4 py-2 px-4 d-inline-flex align-items-center false' and @href = '/?category_id=1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-alt-primary.col.rounded-4.py-2.px-4.d-inline-flex.align-items-center.false</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/following::a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>64a1245a-d33b-44d8-943f-0fb52e15c17f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-alt-primary col rounded-4 py-2 px-4 d-inline-flex align-items-center false</value>
      <webElementGuid>1ab6eb2d-1e61-40ea-b5ab-6ad2df6e21fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/?category_id=1</value>
      <webElementGuid>da602dd0-68d0-4ad5-8528-5b4809478e6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        Hobi
</value>
      <webElementGuid>d17f61e1-b51c-49c5-9772-916a84c7fdb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container&quot;]/nav[@class=&quot;nav row gap-4 px-2 px-lg-0&quot;]/a[@class=&quot;btn btn-alt-primary col rounded-4 py-2 px-4 d-inline-flex align-items-center false&quot;]</value>
      <webElementGuid>5c2965b6-36b1-4d35-bef6-f54cfac4c361</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/following::a[2]</value>
      <webElementGuid>61462e01-f398-41b3-a23e-3e324320634d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Hobi']/parent::*</value>
      <webElementGuid>6c3651c3-fa2a-4fba-a085-de198bee2ded</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/?category_id=1')]</value>
      <webElementGuid>f5fa41cc-ebad-4b71-96c0-6cc8bbe7676d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>c3abb408-bfdb-40c0-91bc-cfcfb663000c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/?category_id=1' and (text() = '
        
        Hobi
' or . = '
        
        Hobi
')]</value>
      <webElementGuid>7f32c4e0-ac92-436c-bde4-7a73bb9f2cab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
