<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Berhasil terjual</name>
   <tag></tag>
   <elementGuidId>05f8c6bb-334a-41f7-903e-8ed72bc96738</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              ' or . = '
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='statusModal19518']/div/form/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.form-check</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9b45193c-306d-4772-ae1f-58ca2c85bb52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check</value>
      <webElementGuid>3eb4ab7a-348b-440e-a48c-a7c924760333</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              </value>
      <webElementGuid>260c861a-0493-43bf-901f-96dbfbabe2a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusModal19518&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/form[@class=&quot;modal-content px-4 py-2&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;form-check&quot;]</value>
      <webElementGuid>35a4575b-653a-4309-bb6b-5671414de4ee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='statusModal19518']/div/form/div[2]/div</value>
      <webElementGuid>0c21e135-aa44-4c25-8679-d3cddf04cb73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perbarui status penjualan produkmu'])[1]/following::div[1]</value>
      <webElementGuid>472d9e25-6516-470c-bf87-6855e28eef7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/following::div[5]</value>
      <webElementGuid>2dcf99c0-6e09-478f-9e69-eb5ec8f39376</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batalkan transaksi'])[1]/preceding::div[1]</value>
      <webElementGuid>3109275a-3810-4f78-bd4c-9d77f498530b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/div</value>
      <webElementGuid>e300b87d-ccc0-421c-96e1-81865b3c35f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              ' or . = '
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              ')]</value>
      <webElementGuid>bc65a408-32ac-4c26-b5b6-8cee9a7b2b5a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
