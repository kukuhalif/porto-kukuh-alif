<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Berhasil terjual</name>
   <tag></tag>
   <elementGuidId>b04b0c5e-93fe-41a6-a878-03b573dad0ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.form-check-label.mb-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='statusModal26278']/div/form/div[2]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>e9fd7eae-28fb-4b31-8fe0-9dff87e75a17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-label mb-2</value>
      <webElementGuid>753d69a0-89cd-4d74-852b-e643dc30fe13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>offer_status_finished</value>
      <webElementGuid>ca6cf666-c758-448f-84dd-e49fbf1ea451</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Berhasil terjual
</value>
      <webElementGuid>cc2107a0-ccad-49f3-8afb-ac5c8d69cb60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusModal26278&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/form[@class=&quot;modal-content px-4 py-2&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;form-check&quot;]/label[@class=&quot;form-check-label mb-2&quot;]</value>
      <webElementGuid>728714ae-b776-44f0-a7a8-2a4be08117ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='statusModal26278']/div/form/div[2]/div/label</value>
      <webElementGuid>8a4a9534-2707-48c0-b68d-a53b9cead863</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perbarui status penjualan produkmu'])[1]/following::label[1]</value>
      <webElementGuid>c842d735-0895-4b83-9dfa-88c24aa6670d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/following::label[1]</value>
      <webElementGuid>1e05fceb-f9d0-47f9-8f38-abd50bf38f0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batalkan transaksi'])[1]/preceding::label[1]</value>
      <webElementGuid>4f16d753-e5f9-42a8-9b44-5d1338c0409b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk diterima'])[2]/preceding::label[2]</value>
      <webElementGuid>bc611941-cdfa-494e-ae7b-b2abed355356</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Berhasil terjual']/parent::*</value>
      <webElementGuid>d2a1944b-28d2-4ef7-98f6-1b3d34341c7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label</value>
      <webElementGuid>d5dd1aae-6d3f-4bbc-b141-8781f40c7c23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                  Berhasil terjual
' or . = '
                  Berhasil terjual
')]</value>
      <webElementGuid>e626fb1a-da48-44ad-a45c-367053e1fa3a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
