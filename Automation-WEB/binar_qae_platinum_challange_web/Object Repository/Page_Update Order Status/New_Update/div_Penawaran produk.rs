<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Penawaran produk</name>
   <tag></tag>
   <elementGuidId>0b0be8a8-4c73-44b7-9583-1359fdebea72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li:nth-of-type(5) > a.notification.my-4.px-2.position-relative > div.notification-body.me-4 > div.notification-subject.fs-6.text-black-50</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[5]/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>55e7b514-19dc-4b2a-930c-3cb38d429bab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification-subject fs-6 text-black-50</value>
      <webElementGuid>724e8484-1072-41c3-9a12-6bc04571969c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Penawaran produk</value>
      <webElementGuid>00e8727b-8fdf-4826-8e69-d9a206a77a1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[5]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]/div[@class=&quot;notification-body me-4&quot;]/div[@class=&quot;notification-subject fs-6 text-black-50&quot;]</value>
      <webElementGuid>a69ac065-e303-4975-b3d5-fd50b73434b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[5]/a/div/div</value>
      <webElementGuid>34c03fad-24c4-4142-9f64-75b4dd465335</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[3]/following::div[2]</value>
      <webElementGuid>9b201e77-4d53-4526-bde8-5b007090e348</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BookRp 100.000'])[1]/following::div[3]</value>
      <webElementGuid>8fc5a88f-b645-4a3a-8e6b-7e5afe81e765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BookRp 100.000Ditawar Rp 14.000'])[1]/preceding::div[1]</value>
      <webElementGuid>7fc491d9-d7a5-4679-9980-8caed1167d97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[2]/preceding::div[3]</value>
      <webElementGuid>144b0fc3-dee7-40b8-b4e2-fe93227f7ad7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Penawaran produk']/parent::*</value>
      <webElementGuid>e483be3a-7c65-4603-a200-6eb5333a4780</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/a/div/div</value>
      <webElementGuid>83500966-8289-4ad7-9520-3ee6e2d43422</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Penawaran produk' or . = 'Penawaran produk')]</value>
      <webElementGuid>05260365-f4b2-4fae-a39a-373651a7c5e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
