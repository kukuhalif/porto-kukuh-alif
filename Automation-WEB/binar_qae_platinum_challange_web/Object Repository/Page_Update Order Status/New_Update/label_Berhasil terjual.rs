<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Berhasil terjual</name>
   <tag></tag>
   <elementGuidId>0098f9fc-5daf-4e38-a928-ea506f19b216</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.form-check-label.mb-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='statusModal26296']/div/form/div[2]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>60a3520e-118f-494a-8d40-c6a1b756115f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-label mb-2</value>
      <webElementGuid>8fa62b9c-db1e-489e-86f6-ca716d7f33c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>offer_status_finished</value>
      <webElementGuid>6ae5926b-9a21-4da2-b3a2-10f75047d331</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Berhasil terjual
</value>
      <webElementGuid>92d73218-a0bf-416f-83ba-a920cc08b0a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusModal26296&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/form[@class=&quot;modal-content px-4 py-2&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;form-check&quot;]/label[@class=&quot;form-check-label mb-2&quot;]</value>
      <webElementGuid>f271e842-1343-462f-b335-6b0b9d8150cd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='statusModal26296']/div/form/div[2]/div/label</value>
      <webElementGuid>fdac01f2-b7ea-432b-b301-8d6a768fd3e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perbarui status penjualan produkmu'])[1]/following::label[1]</value>
      <webElementGuid>f8e618de-9234-44dc-a0de-df3b246e424a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/following::label[1]</value>
      <webElementGuid>5fd61a6e-9e7e-44b1-a514-61bc3218aa94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batalkan transaksi'])[1]/preceding::label[1]</value>
      <webElementGuid>53d6c7f0-57e0-49da-9a46-9034db5a4a7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk diterima'])[2]/preceding::label[2]</value>
      <webElementGuid>4bdfaa51-9966-4df5-a8f9-7f3449c15b5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Berhasil terjual']/parent::*</value>
      <webElementGuid>3dd6213e-4bd7-4b7a-a1af-31244e69fa03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label</value>
      <webElementGuid>7c85ae7c-4ed0-448f-9a54-f32b0bf4c60c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                  Berhasil terjual
' or . = '
                  Berhasil terjual
')]</value>
      <webElementGuid>ca8d03cf-815b-4512-a339-2227695155f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
