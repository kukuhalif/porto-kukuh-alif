<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_card</name>
   <tag></tag>
   <elementGuidId>7744d092-4b7f-4ac3-bc09-a268a67232a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Solo'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li > a.notification.my-4.px-2.position-relative</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>296fd997-18d6-4b36-9fd8-7cf2436d4d72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification my-4 px-2 position-relative</value>
      <webElementGuid>9b06e2a2-fd3c-4eae-8dd6-20d9f6051c0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-id</name>
      <type>Main</type>
      <value>38990</value>
      <webElementGuid>48bc663e-6aa7-4dd2-9c98-1b163cb9568e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-read</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>82e29a14-b475-47ec-9975-183f899bfefd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/users/5776/offers</value>
      <webElementGuid>995d9f5f-a5d2-4e2b-b014-854bea8ad157</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
            
              Penawaran produk
              Laptop Asus ROGRp 222Ditawar Rp 444
            
            25 Jul, 19:58

              
                New alerts
              
</value>
      <webElementGuid>983436bd-3e74-42cf-b1eb-e358d332e28d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[1]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]</value>
      <webElementGuid>89a4104c-56dd-43f0-9b43-6583e77c2741</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Solo'])[1]/following::a[1]</value>
      <webElementGuid>43768804-059e-46ca-b7d0-c95f049e3240</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/users/5776/offers')])[5]</value>
      <webElementGuid>4e763ae0-a522-4534-978e-8599dd43ce57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li/a</value>
      <webElementGuid>aa01267c-f25f-48cd-8a01-ed6868dbdecf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/users/5776/offers' and (text() = '
              
            
              Penawaran produk
              Laptop Asus ROGRp 222Ditawar Rp 444
            
            25 Jul, 19:58

              
                New alerts
              
' or . = '
              
            
              Penawaran produk
              Laptop Asus ROGRp 222Ditawar Rp 444
            
            25 Jul, 19:58

              
                New alerts
              
')]</value>
      <webElementGuid>1ee6ce62-5812-46a5-a267-56c2432a1431</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
