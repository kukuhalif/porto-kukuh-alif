<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Lihat semua notifikasi</name>
   <tag></tag>
   <elementGuidId>aec18b24-fadb-4f9e-9a53-b4f8445bc841</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[18]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-link.text-decoration-none</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6bb08ff9-0382-4a04-8ce7-1673010f6030</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-link text-decoration-none</value>
      <webElementGuid>15b7bc38-9836-49c4-81b3-c7d12dcab53a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/notifications</value>
      <webElementGuid>b17013c7-ecff-45b5-9bfc-2f8a5475b2e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Lihat semua notifikasi
</value>
      <webElementGuid>62129a81-8aab-4199-9811-27d58b2db81d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[@class=&quot;p-2 text-center&quot;]/a[@class=&quot;btn btn-link text-decoration-none&quot;]</value>
      <webElementGuid>728b303c-f4ea-4ce2-9592-8a4aa066080d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[18]/a</value>
      <webElementGuid>187e646c-c328-45b2-97d2-4d5720acdfdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lihat semua notifikasi')]</value>
      <webElementGuid>3fbb20f1-4bb4-4f51-a753-055e40b66042</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Laptop Asus ROGRp 21.000.000'])[1]/following::a[1]</value>
      <webElementGuid>f2bd2108-6fde-401a-a12e-06dd117f3b75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil di terbitkan'])[4]/following::a[1]</value>
      <webElementGuid>4d7fce63-d8c6-495a-a5c6-ef489dbcc8a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[1]/preceding::a[1]</value>
      <webElementGuid>b9e6bef9-dc0a-4558-9f33-892d6363fc15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/preceding::a[2]</value>
      <webElementGuid>1f5abfb5-77d1-4043-9592-4ab137f5a5cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lihat semua notifikasi']/parent::*</value>
      <webElementGuid>b1a599c0-f271-4fb8-b572-2365d2a6ab55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/notifications')])[2]</value>
      <webElementGuid>efaecb54-368d-4c46-a63f-dbc691f6e4b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[18]/a</value>
      <webElementGuid>04f65c5f-ca20-467c-824b-772a5dc31865</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/notifications' and (text() = '
                Lihat semua notifikasi
' or . = '
                Lihat semua notifikasi
')]</value>
      <webElementGuid>25783c37-11fa-4692-bdc5-af60deb71186</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
