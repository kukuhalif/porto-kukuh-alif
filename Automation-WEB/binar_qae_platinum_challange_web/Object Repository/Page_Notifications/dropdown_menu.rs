<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_menu</name>
   <tag></tag>
   <elementGuidId>4a10da9a-78a0-4035-95f4-07c641ff0a32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.dropdown-menu.notification-dropdown-menu.px-4.show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>0135760e-19c2-45bc-9824-10671da5e714</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-menu notification-dropdown-menu px-4 show</value>
      <webElementGuid>42ee822a-d92f-4fa4-bf41-f462460df148</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-popper</name>
      <type>Main</type>
      <value>static</value>
      <webElementGuid>74de875d-928b-4b6c-b87b-c6616b467b0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          
            
                
              
                Penawaran produk
                PutauRp 10.000.000Ditawar Rp 8.888.888
              
              18 May, 19:41

          

            
          
            
                
              
                Berhasil di terbitkan
                PutauRp 100.000.000
              
              18 May, 19:35

          

            
              
                Lihat semua notifikasi
            
    </value>
      <webElementGuid>594ac812-caf7-4956-8c30-4008755be49a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]</value>
      <webElementGuid>3a8fc4a2-421f-49a3-8714-ed281911edf3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      <webElementGuid>09ecee80-66c5-4380-bb82-6d497611ab06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifikasi'])[1]/following::ul[1]</value>
      <webElementGuid>bb3b0c7b-a1bf-4374-964c-66a7c149159c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::ul[1]</value>
      <webElementGuid>a3498fa5-bcac-4c33-bc53-ac6ad176dec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul</value>
      <webElementGuid>c5c6a880-84e7-4e97-91d3-83750e4f02cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = '
          
            
                
              
                Penawaran produk
                PutauRp 10.000.000Ditawar Rp 8.888.888
              
              18 May, 19:41

          

            
          
            
                
              
                Berhasil di terbitkan
                PutauRp 100.000.000
              
              18 May, 19:35

          

            
              
                Lihat semua notifikasi
            
    ' or . = '
          
            
                
              
                Penawaran produk
                PutauRp 10.000.000Ditawar Rp 8.888.888
              
              18 May, 19:41

          

            
          
            
                
              
                Berhasil di terbitkan
                PutauRp 100.000.000
              
              18 May, 19:35

          

            
              
                Lihat semua notifikasi
            
    ')]</value>
      <webElementGuid>8c1a860e-dfe6-4bd1-b2cd-c8cb643d2bad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
