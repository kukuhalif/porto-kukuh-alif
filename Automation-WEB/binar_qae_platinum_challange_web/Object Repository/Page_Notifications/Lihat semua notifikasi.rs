<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lihat semua notifikasi</name>
   <tag></tag>
   <elementGuidId>7b01ca69-742a-48af-9e9f-bcad3800036f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-link.text-decoration-none</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[20]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>85a18ba4-1481-42c5-b514-b135fbf0d109</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-link text-decoration-none</value>
      <webElementGuid>2c60f4cd-63ef-4bdd-9dba-105fd94fe347</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/notifications</value>
      <webElementGuid>3dba7ec9-ed4d-43ce-9e4b-5a8c05afe192</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Lihat semua notifikasi
</value>
      <webElementGuid>7b2c54e5-27e1-41b3-ab34-48baac2b8f3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[@class=&quot;p-2 text-center&quot;]/a[@class=&quot;btn btn-link text-decoration-none&quot;]</value>
      <webElementGuid>a69ea47b-f5d1-4596-b493-61a6cb8672c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[20]/a</value>
      <webElementGuid>5973bb6d-db67-430a-8909-35a82fc512b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lihat semua notifikasi')]</value>
      <webElementGuid>edc1f069-9ecf-4c3c-b235-37330617ffe5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='bookRp 100.000Ditawar Rp 0'])[3]/following::a[1]</value>
      <webElementGuid>731d01a8-5faf-4392-8985-b2dda2300613</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[4]/following::a[1]</value>
      <webElementGuid>7d94a39d-8826-4da2-8730-82572c1aab74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[1]/preceding::a[1]</value>
      <webElementGuid>3feb8226-c1b0-4301-bf83-1233a44e77a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/preceding::a[2]</value>
      <webElementGuid>49063db6-d47c-418a-9375-ddfa45c59757</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/notifications')])[2]</value>
      <webElementGuid>2a7a1905-f8c1-409a-be37-16a34e6d809c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[20]/a</value>
      <webElementGuid>4cee764c-d1c6-4012-aa93-5b9d967664f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/notifications' and (text() = '
                Lihat semua notifikasi
' or . = '
                Lihat semua notifikasi
')]</value>
      <webElementGuid>14dc753b-14ab-45ca-8528-6d9f7fc9fa91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
