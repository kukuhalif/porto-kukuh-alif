<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Lengkapi Info Akun_navbar-brand px-5 py-3</name>
   <tag></tag>
   <elementGuidId>65f064fe-af75-4af8-8da6-a87521dd57fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.navbar-brand.px-5.py-3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(@href, '/')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>23fdcc7d-0a11-4dab-b56f-bababcd5b416</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-brand px-5 py-3</value>
      <webElementGuid>4948c562-48be-4a12-9789-c388b3bcac71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/</value>
      <webElementGuid>7c7ad930-1b27-4609-a323-554496a4aba5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]/a[@class=&quot;navbar-brand px-5 py-3&quot;]</value>
      <webElementGuid>035ac3f9-1926-4493-aa54-d6cfea858093</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/')]</value>
      <webElementGuid>cfa93b61-e963-4a70-b5ea-bec473612628</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>35a1d58c-2b3e-49cc-ae9c-fe32d7249e16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/']</value>
      <webElementGuid>52bc2016-6107-41ee-8a72-c0d022b436b4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
