<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Jual</name>
   <tag></tag>
   <elementGuidId>c33bd9e2-4470-4346-9703-4fb205608c8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.sticky-bottom.d-flex.align-items-center.justify-content-center.py-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Next →'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>467c9203-bd2a-4c0f-870c-8b06e1b6737f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sticky-bottom d-flex align-items-center justify-content-center py-4</value>
      <webElementGuid>a72ea65d-64c1-4c33-802d-7c95149d7692</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  
    
    Jual
</value>
      <webElementGuid>d451e7d8-075b-4a83-814d-995979d1dae0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;sticky-bottom d-flex align-items-center justify-content-center py-4&quot;]</value>
      <webElementGuid>a3e120f1-bde9-4d65-9dd2-d344971986ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next →'])[1]/following::div[1]</value>
      <webElementGuid>09b76f8f-bd8a-4104-b80e-1533b585b750</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='← Previous'])[1]/following::div[1]</value>
      <webElementGuid>9f869a2b-f0f7-4763-9fc0-b97c3eb40d9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div</value>
      <webElementGuid>1d30a4fe-764f-4ae3-8fff-fbad217f6b1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
  
    
    Jual
' or . = '
  
    
    Jual
')]</value>
      <webElementGuid>068eb97f-8b6d-4e98-adec-66a094ba6065</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
