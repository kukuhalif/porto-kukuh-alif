<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_deskripsi_produk</name>
   <tag></tag>
   <elementGuidId>a629ed57-2071-45f1-a11b-d5da553190ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//textarea[@id='product_description']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//textarea[@id='product_description']</value>
      <webElementGuid>962b9479-6f13-4d92-a574-9f7fead90d02</webElementGuid>
   </webElementProperties>
</WebElementEntity>
