<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_warning</name>
   <tag></tag>
   <elementGuidId>ff6dbf52-24db-42ef-b19a-079f1f3c5cc7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-text text-danger']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-text text-danger</value>
      <webElementGuid>ca8ee3fd-da1a-4358-9418-d8da70ac3d33</webElementGuid>
   </webElementProperties>
</WebElementEntity>
