<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>upload_image</name>
   <tag></tag>
   <elementGuidId>f9e24856-732d-4473-8a7e-f0f769b8515b</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@multiple='multiple' and @type='file']</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@multiple='multiple' and @type='file']</value>
      <webElementGuid>d1e1dd66-cdf8-455b-be8b-5b410b7bd821</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bi bi-plus display-6</value>
      <webElementGuid>e0ea718e-3de1-4373-b00f-d0ff975f7f4a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
