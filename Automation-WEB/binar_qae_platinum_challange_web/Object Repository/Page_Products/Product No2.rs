<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Product No2</name>
   <tag></tag>
   <elementGuidId>d6b1d087-856d-4247-8c74-dad52776e9bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='col-12 col-lg-4'][3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='col-12 col-lg-4'][3]</value>
      <webElementGuid>dcdc3ea6-cff0-4aa4-a3f8-3e4b4b30e344</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>e1d1b7ad-49a1-46f8-98fe-26a98a66b785</webElementGuid>
   </webElementProperties>
</WebElementEntity>
