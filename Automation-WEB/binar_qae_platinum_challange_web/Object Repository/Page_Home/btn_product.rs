<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_product</name>
   <tag></tag>
   <elementGuidId>09e08322-a8f9-4511-906e-a6911a3dfdd5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class = 'card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div/a/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card.px-0.border-0.shadow.h-100.pb-4.rounded-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d8b7b40a-63b0-45e8-8083-07f896db23a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>7fe776bc-f29f-475f-a2ad-ea7859cf5cb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      a
      Kendaraan
      Rp 1
    
  </value>
      <webElementGuid>dd34a59f-f742-4e4f-a5e6-3d60162e9da4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>473e4387-b29c-4c39-b28c-d8de774b3aa5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div/a/div</value>
      <webElementGuid>9a5f69ee-bbf0-4500-8f9a-0f595f27a11d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/a/div</value>
      <webElementGuid>1fe8d43d-380e-4806-ab19-1e9a5f47b9fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      a
      Kendaraan
      Rp 1
    
  ' or . = '
      

    
      a
      Kendaraan
      Rp 1
    
  ')]</value>
      <webElementGuid>48f6fe61-3b7b-4d86-8682-5b36ecfe0984</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
