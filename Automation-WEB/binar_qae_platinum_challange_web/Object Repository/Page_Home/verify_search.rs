<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_search</name>
   <tag></tag>
   <elementGuidId>1ce31df2-33a7-4918-865d-09bf3845a6cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'card-title']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div[4]/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5ed5771d-da9f-4480-a8cf-1af53b954078</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title</value>
      <webElementGuid>82057a0f-2249-45f0-a72a-da12c291649d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      HP Laptop 240 G8
      Elektronik
      Rp 13.500.000
    </value>
      <webElementGuid>fd7dafb9-6fc2-4028-9aa3-c3f0c1df37a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]</value>
      <webElementGuid>fb46b977-e989-4695-8d1f-e559f1c61c73</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div[4]/a/div/div</value>
      <webElementGuid>1c90bd7c-8396-4122-abdd-ec78eb9d2621</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.700.000'])[3]/following::div[3]</value>
      <webElementGuid>0721cc14-f7d7-469a-8c44-30391fa01636</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AXIOO Laptop Notebook'])[3]/following::div[3]</value>
      <webElementGuid>15b21158-0b07-4ce0-a892-f1deeaa1ee83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a/div/div</value>
      <webElementGuid>766ceac3-b3d3-4187-9f98-027a62e13f50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      HP Laptop 240 G8
      Elektronik
      Rp 13.500.000
    ' or . = '
      HP Laptop 240 G8
      Elektronik
      Rp 13.500.000
    ')]</value>
      <webElementGuid>44864921-70b5-41d0-be8d-4b83eba22a78</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
