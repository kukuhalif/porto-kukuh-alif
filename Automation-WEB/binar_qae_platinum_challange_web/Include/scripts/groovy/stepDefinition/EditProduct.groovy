package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class EditProduct {

	@Given("the seller is on Edit Product Page")
	public void the_seller_is_on_Edit_Product_Page() {
		WebUI.click(findTestObject('Object Repository/Page_Home/navbar_products'))
	}

	@When("seller click a product card")
	public void seller_click_a_product_card() {
		WebUI.click(findTestObject('Object Repository/Page_Products/Product No2'))
	}

	@When("click edit button")
	public void click_edit_button() {
		WebUI.click(findTestObject('Object Repository/Page_ProductDetails/btn_edit_product'))
	}

	@When("seller edit the product name {string}, with price {string}, type of category {string}, with {string}, and photos")
	public void seller_edit_the_product_and_photos(String name, String price, String category, String description) {
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Product/text_nama_produk'), name)
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Product/text_harga_produk'), price)
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Product/list_kategori'), category, false)
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Product/text_deskripsi_produk'), description)
		GlobalVariable.expected_name = name
		GlobalVariable.expected_category = category
		GlobalVariable.expected_description = description
	}

	@When("click submit button")
	public void click_submit_button() {
		WebUI.click(findTestObject('Object Repository/Page_Edit_Product/btn_terbitkan'))
	}

	@Then("Page redirects to the Product Preview page with the updated field.")
	public void page_redirects_to_the_Product_Preview_page_with_the_updated_field() {
		String name = WebUI.getText(findTestObject('Object Repository/Page_ProductDetails/label_product_title'))
		String category = WebUI.getText(findTestObject('Object Repository/Page_ProductDetails/label_product_category'))
		String description = WebUI.getText(findTestObject('Object Repository/Page_ProductDetails/label_description'))
		WebUI.verifyMatch(name, GlobalVariable.expected_name, false)
		WebUI.verifyMatch(category, GlobalVariable.expected_category, false)
		WebUI.verifyMatch(description, GlobalVariable.expected_description, false)
	}

	@When("seller edit the product details.")
	public void seller_edit_the_product_details() {
		WebUI.sendKeys(findTestObject('Object Repository/Page_Edit_Product/text_nama_produk'), ' -edit')
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Product/text_harga_produk'), '799000')
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Product/list_kategori'), 'Hobi', false)
		WebUI.sendKeys(findTestObject('Object Repository/Page_Edit_Product/text_deskripsi_produk'), ' -edit')
		WebUI.uploadFile(findTestObject('Object Repository/Page_Edit_Product/upload_image'), GlobalVariable.productPhoto)
	}

	@When("seller leaves the product {string} empty")
	public void seller_leaves_the_product_empty(String field) {
		String testObject;
		if (field == "nama_produk" || field == "harga_produk" || field == "deskripsi_produk") {
			testObject = "Object Repository/Page_Edit_Product/text_" + field
			WebUI.clearText(findTestObject(testObject))
		} else if (field == "category") {
			WebUI.selectOptionByIndex(findTestObject('Object Repository/Page_Edit_Product/list_kategori'), 0, FailureHandling.STOP_ON_FAILURE)
		} else {
			WebUI.click(findTestObject('Object Repository/Page_Edit_Product/btn_delete_photo_produk'))
		}
	}

	@Then("Edit product details failed and {string} is displayed.")
	public void edit_product_details_failed_and_is_displayed(String error_message) {
		assert WebUI.getText(findTestObject('Object Repository/Page_Edit_Product/text_warning')) == error_message
	}

	@When("seller logout from the website")
	def seller_logout_from_the_website() {
		WebUI.callTestCase(findTestCase('Pages/Home/Click Profile button'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Home/Click Logout button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When ("seller back to the Product Details page")
	def seller_back_to_product_details() {
		WebUI.back()
		WebUI.refresh()
	}

	@Then("Edit and Delete button is not displayed due to seller need to login before edit the product")
	def verify_edit_and_delete_button() {
		WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_ProductDetails/btn_edit_product'), 5)
		WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_ProductDetails/btn_delete'), 5)
	}
}

