 package stepDefinition
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.But
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable
class AddProduct {
	//Background
	@Given("the seller is on Add Product Page")
	def the_seller_is_on_Add_Product_Page() {
		WebUI.delay(2)
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/products/new');
	}

	//Positive Test
	@When("the seller inputs {string}, {string}, {string}, {string}, and uploads a photo")
	def the_seller_inputs(String product_name, String product_price, String category, String description) {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input mandatory fields'), [
			('input_name') : product_name,
			('input_price') : product_price,
			('input_category') : category,
			('input_description') : description
		], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Pages/Add Product/Upload photo'), [:], FailureHandling.STOP_ON_FAILURE)

		GlobalVariable.expected_name = product_name
		GlobalVariable.expected_price = product_price
		GlobalVariable.expected_category = category
		GlobalVariable.expected_description = description
	}

	@When("the seller clicks submit button")
	def the_seller_clicks_submit_button() {
		WebUI.click(findTestObject('Object Repository/Page_AddProduct/btn_Submit'))
	}

	@Then("the page navigates to the product details page")
	def the_page_navigates_to_the_product_details_page() {
		WebUI.callTestCase(findTestCase('Pages/Product Details/Verify content'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("the product is added with the correct information")
	def the_product_is_added_with_the_correct_information() {
		WebUI.callTestCase(findTestCase('Pages/Product Details/Verify product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	//Negative Test
	@When("the seller inputs all the mandatory fields")
	def the_seller_inputs_all_the_mandatory_fields() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input mandatory fields'),[
			('categoryIndex') : '1', ('name') : 'Book', ('price') : '100000',
			('description') : 'New book, 100 pages'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Add Product/Upload photo'), [:], FailureHandling.STOP_ON_FAILURE)
	};

	@But("the seller leaves {string} empty")
	def the_seller_leaves(String emptyField) {
		String testObject;
		if (emptyField == "product_name" || emptyField == "product_price" || emptyField == "description") {
			testObject = "Object Repository/Page_AddProduct/textfield_" + emptyField
			WebUI.clearText(findTestObject(testObject))
		} else if (emptyField == "category") {
			WebUI.selectOptionByIndex(findTestObject('Object Repository/Page_AddProduct/select_category'), 0, FailureHandling.STOP_ON_FAILURE)
		} else {
			WebUI.click(findTestObject('Page_AddProduct/btn_delete_photo'))
		}
	}

	@Then("error message {string} is displayed")
	def error_message_is_displayed(String expectedMessage) {
		def actualMessage = WebUI.getText(findTestObject('Page_AddProduct/text_error_message'))
		WebUI.verifyMatch(actualMessage, expectedMessage, false)
	}
}