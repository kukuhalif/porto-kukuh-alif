 package stepDefinition
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.But
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable
class PreviewProduct {
	@When("the seller inputs product_name, product_price, category, description, and uploads a photo")
	def seller_input_product_details() {
		WebUI.setText(findTestObject('Object Repository/Page_AddProduct/textfield_product_name'), 'Buku Tulis')
		WebUI.setText(findTestObject('Object Repository/Page_AddProduct/textfield_product_price'), '99000')
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_AddProduct/select_category'), 'Hobi', false)
		WebUI.setText(findTestObject('Object Repository/Page_AddProduct/textfield_description'), 'Buku Tulis yang bisa membuat pintar')
		WebUI.uploadFile(findTestObject('Page_AddProduct/input_Photo'), GlobalVariable.productPhoto)
	}

	@When("the seller clicks preview button")
	def click_preview() {
		WebUI.click(findTestObject('Object Repository/Page_AddProduct/btn_preview'))
	}

	@Then("the page navigates to the product preview page")
	def navigate_preview_page() {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProductDetails/text_product_name'))
		WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProductDetails/text_product_price'))
		WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProductDetails/text_product_description'))
	}
	
	@When("the seller edit product_name, product_price, category, description, and uploads a photo")
	def seller_edit_product_details() {
		WebUI.sendKeys(findTestObject('Object Repository/Page_Edit_Product/text_nama_produk'), ' -edited')
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Product/text_harga_produk'), '799000')
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Product/list_kategori'), 'Kendaraan', false)
		WebUI.sendKeys(findTestObject('Object Repository/Page_Edit_Product/text_deskripsi_produk'), ' -edited')
		WebUI.uploadFile(findTestObject('Object Repository/Page_Edit_Product/upload_image'), GlobalVariable.productPhoto)
	}
	
}