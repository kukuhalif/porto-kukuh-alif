package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Catalog {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("User click Baju {string} category button")
	public void user_click_Baju_category_button(String Baju) {
		WebUI.click(findTestObject('Page_Catalog/btn_Baju'))
		Baju = WebUI.getText(findTestObject('Page_Catalog/Categor_ Catalog'))
	}

	@Then("User can see item on sale in Baju {string} category")
	public void user_can_see_item_on_sale_in_Baju_category(String Baju) {
		WebUI.verifyMatch(Baju, Baju, false)
		WebUI.delay(2)
	}

	@When("User click Hobi {string} category button")
	public void user_click_Hobi_category_button(String Hobi) {
		WebUI.click(findTestObject('Page_Catalog/btn_Hobi'))
		Hobi = WebUI.getText(findTestObject('Page_Catalog/Categor_ Catalog'))
	}

	@Then("User can see item on sale in Hobi {string} category")
	public void user_can_see_item_on_sale_in_Hobi_category(String Hobi) {
		WebUI.verifyMatch(Hobi, Hobi, false)
		WebUI.delay(2)
	}

	@When("User click Kendaraan {string} category button")
	public void user_click_Kendaraan_category_button(String Kendaraan) {
		WebUI.click(findTestObject('Page_Catalog/btn_Kendaraan'))
		Kendaraan = WebUI.getText(findTestObject('Page_Catalog/Categor_ Catalog'))
	}

	@Then("User can see item on sale in Kendaraan {string} category")
	public void user_can_see_item_on_sale_in_Kendaraan_category(String Kendaraan) {
		WebUI.verifyMatch(Kendaraan, Kendaraan, false)

		WebUI.delay(2)
	}

	@When("User click Kesehatan {string} category button")
	public void user_click_Kesehatan_category_button(String Kesehatan) {
		WebUI.click(findTestObject('Page_Catalog/btn_Kesehatan'))

		Kesehatan = WebUI.getText(findTestObject('Page_Catalog/Categor_ Catalog'))
	}

	@Then("User can see item on sale in Kesehatan {string} category")
	public void user_can_see_item_on_sale_in_Kesehatan_category(String Kesehatan) {
		WebUI.verifyMatch(Kesehatan, Kesehatan, false)

		WebUI.delay(2)
	}

	@When("User click  Elektronik {string} category button")
	public void user_click_Elektronik_category_button(String Elektronik) {
		WebUI.click(findTestObject('Page_Catalog/btn_Elektronik'))

		Elektronik = WebUI.getText(findTestObject('Page_Catalog/Categor_ Catalog'))
	}

	@Then("User can see item on sale in Elektronik {string} category")
	public void user_can_see_item_on_sale_in_Elektronik_category(String Elektronik) {
		WebUI.verifyMatch(Elektronik, Elektronik, false)
		WebUI.delay(2)
	}
}