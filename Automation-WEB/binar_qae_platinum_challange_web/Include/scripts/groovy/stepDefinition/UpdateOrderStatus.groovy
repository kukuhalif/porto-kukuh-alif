package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class UpdateOrderStatus {

	@Given("the user is logged in as seller")
	public void the_user_is_logged_in_as_seller() {
		WebUI.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login Seller'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user submit the offer")
	public void user_submit_the_offer() {
		WebUI.click(findTestObject('Page_Notifications/Btn_notification'))

		WebUI.click(findTestObject('Page_Update Order Status/New_Update/div_Penawaran produk'))

		WebUI.click(findTestObject('Page_Update Order Status/New_Update/label_Terima'))
	}

	@When("user want update status product")
	public void user_want_update_status_product() {
		WebUI.click(findTestObject('Page_Update Order Status/Page_SecondHand/button_Status'))

		WebUI.click(findTestObject('Page_Update Order Status/New_Update/label_Berhasil terjual'))

		WebUI.click(findTestObject('Page_Update Order Status/Page_SecondHand/Btn_Kirim'))
	}

	@Then("the product {string}")
	public void the_product(String string) {
		WebUI.verifyElementVisible(findTestObject('Page_Update Order Status/New_Update/Page_SecondHand/h6_Berhasil terjual'))
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/');
	}
}
