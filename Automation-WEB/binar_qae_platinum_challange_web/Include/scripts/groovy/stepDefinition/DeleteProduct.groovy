package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then

public class DeleteProduct {
	@Given("the seller adds a new product")
	def the_seller_adds_a_new_product () {
		WebUI.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Add Product'), [:], FailureHandling.STOP_ON_FAILURE)
	};
	
	@When("the seller deletes the product")
	def the_seller_deletes_the_product () {
		GlobalVariable.productUrl = WebUI.getUrl()
		WebUI.click(findTestObject('Object Repository/Page_ProductDetails/btn_Delete'))
		WebUI.delay(3)
	};

	@Then("the product is deleted")
	def the_product_is_deleted() {
		WebUI.navigateToUrl(GlobalVariable.productUrl);
		WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Home/btn_add_product'));
		def currentUrl = WebUI.getUrl()
		WebUI.verifyMatch(currentUrl, 'https://secondhand.binaracademy.org/', false)
	};
}
