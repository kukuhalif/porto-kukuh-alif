package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Profile {
	@Given("the user is logged in as a user")
	public void the_user_is_logged_in_as_a_user() {
		WebUI.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login User'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user click profile button")
	public void user_click_profile_button() {
		WebUI.callTestCase(findTestCase('Pages/Home/Click Profile button'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Home/Click to Profile button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user can select kota, input alamat, No Handphone, and upload Foto")
	public void user_can_select_kota_input_alamat_No_Handphone_and_upload_Foto() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Select Kota'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.setText(findTestObject('Page_Profile/text_alamat'), 'Jalan Rambutan No 14, Jakarta barat')
		WebUI.setText(findTestObject('Page_Profile/text_No Handphone_userphone_number'), '08562731231')
		WebUI.callTestCase(findTestCase('Pages/Profile/Upload Foto'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("click Simpan")
	public void click_Simpan() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Click Submit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("verify that the profile has been updated")
	public void verify_that_the_profile_has_been_updated() {
		WebUI.callTestCase(findTestCase('Pages/Home/Click My Listing button'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.verifyElementVisible(findTestObject('Page_My_Listing/navbar_My_Listing'))
	}
	@Then("user delete the name from the profile column")
	public void user_delete_the_name_from_the_profile_column() {
		WebUI.setText(findTestObject('Page_Profile/text_Nama_username'), '')
	}

	@And("user fill in kota, alamat, no handphone")
	public void user_fill_in_kota_alamat_no_handphone() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Select Kota'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.setText(findTestObject('Page_Profile/text_alamat'), 'Jalan Rambutan No 14, Jakarta timur')
		WebUI.setText(findTestObject('Page_Profile/text_No Handphone_userphone_number'), '08562731231')
		WebUI.callTestCase(findTestCase('Pages/Profile/Upload Foto'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Profile/Click Submit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user select {string} in column Kota")
	public void user_select_in_column_Kota(String string) {
		WebUI.selectOptionByLabel(findTestObject('Page_Profile/select_kota'), 'Pilih Kota', false)
	}

	@And("user fill in nama, alamat, no handphone")
	public void user_fill_in_nama_alamat_no_handphone() {
		WebUI.setText(findTestObject('Page_Profile/text_Nama_username'), 'Testing1')
		WebUI.setText(findTestObject('Page_Profile/text_alamat'), 'Jalan Rambutan No 14, Jakarta timur')
		WebUI.setText(findTestObject('Page_Profile/text_No Handphone_userphone_number'), '08562731231')
		WebUI.callTestCase(findTestCase('Pages/Profile/Upload Foto'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Profile/Click Submit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user delete alamat from the profile column")
	public void user_delete_alamat_from_the_profile_column() {
		WebUI.setText(findTestObject('Page_Profile/text_alamat'), '')
	}

	@Then("user fill in nama, kota, no handphone")
	public void user_fill_in_nama_kota_no_handphone() {
		WebUI.setText(findTestObject('Page_Profile/text_Nama_username'), 'Testing2')
		WebUI.selectOptionByLabel(findTestObject('Page_Profile/select_kota'), 'Bandung', false)
		WebUI.setText(findTestObject('Page_Profile/text_No Handphone_userphone_number'), '08562731231')
		WebUI.callTestCase(findTestCase('Pages/Profile/Upload Foto'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Profile/Click Submit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user delete no handphone from the profile column")
	public void user_delete_no_handphone_from_the_profile_column() {
		WebUI.setText(findTestObject('Page_Profile/text_No Handphone_userphone_number'), '')
	}

	@Then("user fill in nama, kota, alamat")
	public void user_fill_in_nama_kota_alamat() {
		WebUI.setText(findTestObject('Page_Profile/text_Nama_username'), 'Testing3')
		WebUI.selectOptionByLabel(findTestObject('Page_Profile/select_kota'), 'Jogja', false)
		WebUI.setText(findTestObject('Page_Profile/text_alamat'), 'Jalan Rambutan No 14, Jakarta timur')
		WebUI.callTestCase(findTestCase('Pages/Profile/Upload Foto'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Profile/Click Submit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("Verify that the column must be filled")
	public void Verify_that_the_column_must_be_filled() {
		WebUI.verifyElementHasAttribute(findTestObject('Page_Profile/verify_object_profile'), 'required', 0)
	}
}