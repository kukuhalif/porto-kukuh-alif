package stepDefinition
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.Keys as Keys



class Search {
	@Given("user Input a keyword in the search box")
	public void user_Input_a_keyword_in_the_search_box() {
		WebUI.setText(findTestObject('Page_Home/text_search'), 'motor')
	}

	@When("Press enter on the keyboard")
	public void press_enter_on_the_keyboard() {
		WebUI.sendKeys(findTestObject('Page_Home/text_search'), Keys.chord(Keys.ENTER))
	}

	@Then("product search verification")
	public void product_search_verification() {
		WebUI.scrollToPosition(0, 500)
		WebUI.verifyElementVisible(findTestObject('Page_Home/verify_search'))
		WebUI.verifyMatch(WebUI.getText(findTestObject('Page_Home/verify_search')), '.*Motor.*', true)
	}
}