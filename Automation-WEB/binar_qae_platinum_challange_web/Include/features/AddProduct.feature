@AddProductFeature
Feature: Add Product
  As a seller, I can add product

  Background: 
    #Given the user is logged in as a seller
    And the seller is on Add Product Page

  @TC.Add.001 @PositiveTest
  Scenario Outline: Seller can add new product with valid inputs
    When the seller inputs "<product_name>", "<product_price>", "<category>", "<description>", and uploads a photo
    And the seller clicks submit button
    Then the page navigates to the product details page
    And the product is added with the correct information

    Examples: 
      | product_name | product_price | category | description         |
      | book         |        100000 | Hobi     | new book, 100 pages |

  @TC.Add.004 @TC.Add.005 @TC.Add.006 @TC.Add.007 @NegativeTest
  Scenario Outline: Seller adds a product with empty fields
    When the seller inputs all the mandatory fields
    But the seller leaves "<emptyField>" empty
    And the seller clicks submit button
    Then error message "<expectedMessage>" is displayed

    Examples: 
      | emptyField    | expectedMessage            | #TestCaseID |
      | product_name  | Name can't be blank        | #TC.Add.004 |
      | product_price | Price can't be blank       | #TC.Add.005 |
      | category      | Category must exist        | #TC.Add.006 |
      | description   | Description can't be blank | #TC.Add.007 |
