
@Register
Feature: User register the website
  As a user I want to register the secondhand website

  @TC.Reg.001
  Scenario Outline: User register website
    Given I want to open register page
    When Fill my informations
    Then I verify the <status> in step

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |