@Profile
Feature: Profile
  User complete profile

  @TC.Acc.003 @NegativeTest
  Scenario: User can't update account info without name
    #Given the user is logged in as a user
    When user click profile button
    Then user delete the name from the profile column
    And user fill in kota, alamat, no handphone
    And click Simpan
    Then Verify that the column must be filled

  @TC.Acc.004 @NegativeTest
  Scenario: User can't update account info without city
    Then user select "pilih kota" in column Kota
    And user fill in nama, alamat, no handphone
    And click Simpan
    Then Verify that the column must be filled

  @TC.Acc.005 @NegativeTest
  Scenario: User can't update account info without address
    Then user delete alamat from the profile column
    And user fill in nama, kota, no handphone
    And click Simpan
    Then Verify that the column must be filled

  @TC.Acc.006 @negativeTest
  Scenario: User can't update account info without phone number
    Then user delete no handphone from the profile column
    And user fill in nama, kota, alamat
    And click Simpan
    Then Verify that the column must be filled

  @TC.Acc.001 @PositiveTest
  Scenario: user can complete their account info
    Then user can select kota, input alamat, No Handphone, and upload Foto
    And click Simpan
    Then verify that the profile has been updated
