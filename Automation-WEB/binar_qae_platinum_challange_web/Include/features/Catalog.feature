@Catalog
Feature: Catalog
 as buyer i can see product matching with category

  @TC.Catalog.001
  Scenario: User can item on sale match with category
    When User click Baju "Baju" category button
    Then User can see item on sale in Baju "Baju" category
    When User click Hobi "Hobi" category button
    Then User can see item on sale in Hobi "Hobi" category
    When User click Kendaraan "Kendaraan" category button
    Then User can see item on sale in Kendaraan "Kendaraan" category
    When User click Kesehatan "Kesehatan" category button
    Then User can see item on sale in Kesehatan "Kesehatan" category
    When User click  Elektronik "Elektronik" category button
    Then User can see item on sale in Elektronik "Elektronik" category