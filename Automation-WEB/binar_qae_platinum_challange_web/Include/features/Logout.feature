@Logout
Feature: Logout
As a user, I can log out

  @TC.Lgo.001 @PositiveTest
  Scenario: User can logout
    #Given the user is logged in
    When the user clicks logout button
    Then the user is logged out