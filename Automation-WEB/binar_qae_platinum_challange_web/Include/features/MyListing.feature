@MyListing
Feature: MyListing
As a seller, I can see my product

  Background: 
    #Given the user is logged in as a seller

  @TC.Myl.001-My_Listing
  Scenario: Seller can view product to sell
    When the seller click my listing icon
    Then the seller can see my product