#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template


@EditProduct @smoke
Feature: Edit Product
  As a seller, I want to edit existing product and save the changes

  Background: 
    #Given the user is logged in as a seller
    And the seller is on Edit Product Page

  @TC.Edi.001 @PositiveTest
  Scenario Outline: Seller can edit an existing product and save the changes
    When seller click a product card
    And click edit button
    And seller edit the product name "<name>", with price "<price>", type of category "<category>", with "<description>", and photos
    And click submit button
    Then Page redirects to the Product Preview page with the updated field.

    Examples: 
      | name      | price | category | description       |
      | Baju baru | 99999 | Baju     | new cloth, cotton |
      
  @TC.Edi.002 @TC.Edi.003 @TC.Edi.004 @TC.Edi.005 @TC.Edi.006 @NegativeTest
  Scenario Outline: Seller can't edit product details with blank "<field>"
    When seller click a product card
    And click edit button
    And seller edit the product details.
    But seller leaves the product "<field>" empty
    And click submit button
    Then Edit product details failed and "<error_message>" is displayed.

    Examples: 
      | field            | error_message              |
      | nama_produk      | Name can't be blank        |
      | harga_produk     | Price can't be blank       |
      | category         | Category must exist        |
      | deskripsi_produk | Description can't be blank |
      #| photo            | Photo must exist           |
  
  @TC.Edi.007    
  Scenario: Seller can't edit product details without loging in
    When seller click a product card
    And click edit button
    And seller logout from the website
    And seller back to the Product Details page
    Then Edit and Delete button is not displayed due to seller need to login before edit the product
