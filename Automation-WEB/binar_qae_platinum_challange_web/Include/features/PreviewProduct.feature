Feature: Preview Product
  As a seller, I want to see the preview before submitting the new product or edit the existing product

  Background: 
    #Given the user is logged in as a seller

  @TC.Pre.001 @PositiveTest
  Scenario: Seller can preview the product before submit the new peoduct
    When the seller is on Add Product Page
    And the seller inputs product_name, product_price, category, description, and uploads a photo
    And the seller clicks preview button
    Then the page navigates to the product preview page

  @TC.Pre.002 @PositiveTest
  Scenario: Seller can preview the product after edit existing product
    When the seller is on Edit Product Page
    And seller click a product card
    And click edit button
    And the seller edit product_name, product_price, category, description, and uploads a photo
    And the seller clicks preview button
    Then the page navigates to the product preview page
