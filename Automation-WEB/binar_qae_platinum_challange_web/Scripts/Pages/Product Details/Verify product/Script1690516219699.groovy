import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.text.DecimalFormat as DecimalFormat
import java.text.DecimalFormatSymbols as DecimalFormatSymbols

actual_name = WebUI.getText(findTestObject('Page_ProductDetails/label_product_title'))

actual_price = WebUI.getText(findTestObject('Page_ProductDetails/text_product_price'))

actual_description = WebUI.getText(findTestObject('Object Repository/Page_ProductDetails/text_product_description'))

WebUI.verifyMatch(actual_name, GlobalVariable.expected_name, false)


double double_price = Double.valueOf(GlobalVariable.expected_price)
DecimalFormatSymbols Symbols = new DecimalFormatSymbols()
Symbols.setGroupingSeparator('.' as char) 
DecimalFormat formatter = new DecimalFormat("Rp #,##0", Symbols)
GlobalVariable.expected_price = formatter.format(double_price)

WebUI.verifyMatch(actual_price, GlobalVariable.expected_price, false)

WebUI.verifyMatch(actual_description, GlobalVariable.expected_description, false)

