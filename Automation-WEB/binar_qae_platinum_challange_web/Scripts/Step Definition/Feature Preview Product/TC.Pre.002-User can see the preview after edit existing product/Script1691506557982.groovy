import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.text.DecimalFormat as DecimalFormat
import java.text.DecimalFormatSymbols as dfs

'Memanggil test case login'
WebUI.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login Seller'), [:], FailureHandling.STOP_ON_FAILURE)

'Klik Produk navigasi bar'
WebUI.click(findTestObject('Object Repository/Page_Home/navbar_products'))

'Pilih Product'
WebUI.click(findTestObject('Object Repository/Page_Products/Product No2'))

'klik tombol Edit Product'
WebUI.click(findTestObject('Object Repository/Page_ProductDetails/btn_edit_product'))

'Edit nama produk'
WebUI.sendKeys(findTestObject('Object Repository/Page_Edit_Product/text_nama_produk'), ' -edited')

nama_produk = WebUI.getAttribute(findTestObject('Object Repository/Page_Edit_Product/text_nama_produk'), 'value')

'Edit harga produk'
WebUI.setText(findTestObject('Object Repository/Page_Edit_Product/text_harga_produk'), '799000')

'Edit harga produk'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Product/list_kategori'), 'Kendaraan', false)

'Edit deskripsi produk'
WebUI.sendKeys(findTestObject('Object Repository/Page_Edit_Product/text_deskripsi_produk'), ' -edited')

'Unggah foto produk'
WebUI.uploadFile(findTestObject('Object Repository/Page_Edit_Product/upload_image'), GlobalVariable.productPhoto)

'Klik tombol preview produk'
WebUI.click(findTestObject('Object Repository/Page_Edit_Product/btn_preview'))

'Verifikasi jika nama produk sudah terupdate'
assert nama_produk == WebUI.getText(findTestObject('Object Repository/Page_ProductDetails/label_product_title'))

WebUI.delay(5)