import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.text.DecimalFormat as DecimalFormat
import java.text.DecimalFormatSymbols as dfs

'Memanggil test case login'
WebUI.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login Seller'), [:], FailureHandling.STOP_ON_FAILURE)

'Click button Add Product'
WebUI.click(findTestObject('Page_Home/btn_add_product'))

'Input Product Name'
WebUI.setText(findTestObject('Object Repository/Page_AddProduct/textfield_product_name'), input_name)

'Input Product Price'
WebUI.setText(findTestObject('Object Repository/Page_AddProduct/textfield_product_price'), input_price)

'Choose catogory product'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_AddProduct/select_category'), input_category, false)

'Input Product Description'
WebUI.setText(findTestObject('Object Repository/Page_AddProduct/textfield_description'), input_description)

'Upload photo Product'
WebUI.uploadFile(findTestObject('Page_AddProduct/input_Photo'), GlobalVariable.productPhoto)

'Klik button Preview product'
WebUI.click(findTestObject('Object Repository/Page_AddProduct/btn_preview'))

'Verify element visible product name'
WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProductDetails/text_product_name'))

'Verify element visible product price'
WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProductDetails/text_product_price'))

'Verify element visible product description'
WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProductDetails/text_product_description'))

actual_name = WebUI.getText(findTestObject('Page_ProductDetails/label_product_title'))

actual_price = WebUI.getText(findTestObject('Page_ProductDetails/text_product_price'))

actual_description = WebUI.getText(findTestObject('Object Repository/Page_ProductDetails/text_product_description'))

'Verify element value of product name'
WebUI.verifyMatch(actual_name, expected_name, false)

double double_price = Double.valueOf(expected_price)
java.text.DecimalFormatSymbols dfs = new java.text.DecimalFormatSymbols()
dfs.setGroupingSeparator('.' as char) 
DecimalFormat formatter = new DecimalFormat('###,###', dfs)

expected_price = ('Rp ' + formatter.format(double_price))

'Verify element value of product price'
WebUI.verifyMatch(actual_price, expected_price, false)

'Verify element value of product description'
WebUI.verifyMatch(actual_description, expected_description, false)
