import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login User'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Home/Click Profile button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Home/Click to Profile button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Page_Profile/text_Nama_username'), '')

WebUI.callTestCase(findTestCase('Pages/Profile/Select Kota'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Page_Profile/text_alamat'), 'Jalan Rambutan No 14, Jakarta timur')

WebUI.setText(findTestObject('Page_Profile/text_No Handphone_userphone_number'), '08562731231')

WebUI.callTestCase(findTestCase('Pages/Profile/Upload Foto'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Profile/Click Submit Profile'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementHasAttribute(findTestObject('Page_Profile/verify_object_profile'), 'required', 0)

