import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static org.junit.Assert.*
import org.openqa.selenium.Keys as Keys

'Seller can\'t edit product details without loging in'

'Memanggil test case login'
WebUI.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login Seller'), [:], FailureHandling.STOP_ON_FAILURE)

'Klik Produk navigasi bar'
WebUI.click(findTestObject('Object Repository/Page_Home/navbar_products'))

'Pilih Product'
WebUI.click(findTestObject('Object Repository/Page_Products/Product No2'))

WebUI.click(findTestObject('Object Repository/Page_Home/btn_profile'))

WebUI.click(findTestObject('Object Repository/Page_Home/btn_logout'))

WebUI.delay(5)

WebUI.back()

'Verifikasi tidak tidak ada button edit dan delete produk'
WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_ProductDetails/btn_edit_product'), 5)
WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_ProductDetails/btn_delete'), 5)