import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\emrea\\Downloads\\app-release-second-hand-gcp.apk', true)

Mobile.tap(findTestObject('Object Repository/Register/android.widget.TextView - Akun'), 0)

Mobile.tap(findTestObject('Object Repository/Register/android.widget.Button - Masuk'), 0)

Mobile.tap(findTestObject('Object Repository/Register/android.widget.TextView - Daftar'), 0)

Mobile.sendKeys(findTestObject('Object Repository/Register/android.widget.EditText - Masukkan nama lengkap'), 'dika intan')

int randomNumber

randomNumber = ((Math.random() * 5000) as int)

String randomUserEmail

randomUserEmail = (('register+' + randomNumber) + '@gmail.com')

Mobile.sendKeys(findTestObject('Object Repository/Register/android.widget.EditText - Masukkan email'), randomUserEmail)

Mobile.sendKeys(findTestObject('Object Repository/Register/android.widget.EditText - Masukkan password'), 'password')

Mobile.sendKeys(findTestObject('Object Repository/Register/android.widget.EditText - Contoh 08123456789'), '234234234434')

Mobile.sendKeys(findTestObject('Object Repository/Register/android.widget.EditText - Masukkan kota'), 'jakarta')

Mobile.sendKeys(findTestObject('Object Repository/Register/android.widget.EditText - Masukkan alamat'), 'depok jakarta indonesia')

Mobile.tap(findTestObject('Object Repository/Register/android.widget.TextView - Daftar (1)'), 0)

Mobile.closeApplication()

