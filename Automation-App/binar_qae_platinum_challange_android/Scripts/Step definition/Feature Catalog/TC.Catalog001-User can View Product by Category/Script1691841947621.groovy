import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.delay(60, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Catalog/Komputer dan Aksesoris Category'), 0)

Mobile.delay(60, FailureHandling.STOP_ON_FAILURE)

Komputer dan Aksesoris = Mobile.getText(findTestObject('Catalog/Komputer dan Aksesoris_Text'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyMatch('Komputer dan Aksesoris', 'Komputer dan Aksesoris', false)

Mobile.tap(findTestObject('Catalog/Elektronik Category'), 0)

Mobile.delay(60, FailureHandling.STOP_ON_FAILURE)

Elektronik = Mobile.getText(findTestObject('Catalog/Elektronik_Text'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyMatch('Elektronik', 'Elektronik', false)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

