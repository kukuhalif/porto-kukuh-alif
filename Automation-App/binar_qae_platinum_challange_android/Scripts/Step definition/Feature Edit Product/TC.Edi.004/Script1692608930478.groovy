import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'call test case for login'
WebUI.callTestCase(findTestCase('Test Cases/Step definition/Precondition/Precondition - Login Seller'), [:], FailureHandling.STOP_ON_FAILURE)

'Get device height & width'
device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()

'tap navbar akun'
Mobile.tap(findTestObject('Edit Product/Beranda/toolbar_Akun'), 1, FailureHandling.OPTIONAL)

'Tap menu Daftar_jual_saya'
Mobile.tap(findTestObject('Object Repository/Edit Product/Akun Saya/Daftar_Jual_Saya'), 5)

'Choose product'
Mobile.tap(findTestObject('Object Repository/Edit Product/Daftar Jual Saya/Produk No1'), 60)

'Get product name to store in GlobalVariable'
GlobalVariable.expected_name = Mobile.getText(findTestObject('Object Repository/Edit Product/Ubah Produk/input_namaProduk'), 10)
'Edit nama_produk'
Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/input_namaProduk'), GlobalVariable.expected_name + ' -Edit', 5)

'Edit harga_produk'
Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/Input_hargaProduk'), '999000', 5)

'tap Kategori list'
Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/delete Cat1'), 1)
Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/delete Cat2'), 1, FailureHandling.OPTIONAL)
Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/delete Cat3'), 1, FailureHandling.OPTIONAL)
Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/delete Cat4'), 1, FailureHandling.OPTIONAL)
Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/delete Cat5'), 1, FailureHandling.OPTIONAL)

'Get lokasi to store in GlobalVariable'
GlobalVariable.expected_location = Mobile.getText(findTestObject('Object Repository/Edit Product/Ubah Produk/input_lokasi'), 10)
'Edit lokasi'
Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/input_lokasi'), GlobalVariable.expected_location + ' -Edit', 5)

'Get description to store in Global Variable'
GlobalVariable.expected_description = Mobile.getText(findTestObject('Object Repository/Edit Product/Ubah Produk/Deskripsi'), 10)
'Edit description'
Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/Deskripsi'), GlobalVariable.expected_description + ' -Edit', 5)

'tap button perbarui product'
Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/button_Perbarui Produk'), 5)

'Verify error message visible'
Mobile.verifyElementVisible(findTestObject('Object Repository/Edit Product/Ubah Produk/Verify_No_Categories'), 1)

'Verify value error message'
Mobile.verifyMatch('Pilih minimal 1 kategori', Mobile.getText(findTestObject('Object Repository/Edit Product/Ubah Produk/Verify_No_Categories'), 3), false)

Mobile.delay(10)