@Profile
Feature: User can view account info

  @TC.Acc.002 @NegativeTest
  Scenario: user cant view account info
    When user tap profile button
    Then user cant view account info

  @TC.Acc.001 @PositiveTest
  Scenario: user views account
    #Given the user is logged in as a user
    And user can tap account menu on bottom navbar
    And tap the edit button
    Then verify that it is an information account

  @TC.Acc.009 @NegativeTest
  Scenario: User can't update their name to blank
    Then user can tap edit name account
    And user can remove previous
    And user save blank name and verify update profile failed and error message

  @TC.Acc.003 @PositiveTest
  Scenario: user can edit name account
    Then user can write new name account
    And user save the name and verify name edit already success

  @TC.Acc.004 @PositiveTest
  Scenario: User can update their phone number
    Then user can tap edit Nomor HP account
    And user can remove previous
    Then user can write new Nomor HP account
    And user save the name and verify name edit already success

  @TC.Acc.005 @PositiveTest
  Scenario: User can update their city
    Then user can tap edit kota
    And user can remove previous
    Then user can write new kota
    And user save the name and verify name edit already success

  @TC.Acc.006 @PositiveTest
  Scenario: User can update their address
    Then user can tap edit alamat
    And user can remove previous
    Then user can write new alamat
    And user save the name and verify name edit already success
