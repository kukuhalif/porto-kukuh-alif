@EditProductFeature
Feature: Edit Product
  As a seller, I can edit existing product

  Background: 
    #Given the user is logged in as a seller
    And the seller is on My Listing Page

  @TC.Edi.001 @PositiveTest
  Scenario: User can edit an existing product and save the changes
    When seller click product card
    And seller change product name, price, category, description or location
    And tap button save changes
    Then the product field is successfully updated
