@Transaction
Feature: Transaction Page
  User Can see Transaction Page

  @TC.Transaction.002 @NegativeTest
  Scenario: User Can't See Transaction Item Without Login
    When User tap transaction button 
    Then User can see text must login first

  @TC.Transaction.001 @PositiveTest
  Scenario: User Can See Transaction Item 
  #	Given User login as Buyer
  	When User tap Transaction button
  	Then User Can see Transaction Success
  	Then User Can see Transaction Canceled