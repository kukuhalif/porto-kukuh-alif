@EditProductFeature
Feature: Log out
  As a user, I can log out

  Background: 
    #Given the user is logged in

  @TC.Log.001 @PositiveTest
  Scenario: User can log out
    When user click log out button
   Then user account will be logged out, and redirected to main page
