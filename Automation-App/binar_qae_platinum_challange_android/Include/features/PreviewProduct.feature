@PreviewProductFeature
Feature: Preview Product
  As a seller, I can see the preview page before submitting the new product

  Background: 
    #Given the user is logged in as a seller
    And the seller in add product page

  @TC.Pre.001 @PositiveTest
  Scenario: User can see the preview page before submitting the new product
    When seller click preview button
    Then the preview page is displayed the new product
