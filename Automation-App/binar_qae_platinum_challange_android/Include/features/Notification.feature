@Notification
Feature: Notification
  User can see notification

Background
   #	Given the user is logged in as a seller

  @TC_Not_001
  Scenario Outline: user can see notification drawer
    When the user click notification
    Then user can see section notification

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
