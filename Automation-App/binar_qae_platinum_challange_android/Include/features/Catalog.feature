@Catalog
Feature: Catalog
 as buyer i can see product matching with category

  @TC.Catalog.001
  Scenario: User can item on sale match with category
    When User click Komputer dan Aksesoris "Komputer dan Aksesoris" category button
    Then User can see item on sale in Komputer dan Aksesoris "Komputer dan Aksesoris" category
    When User click  Elektronik "Elektronik" category button
    Then User can see item on sale in Elektronik "Elektronik" category