package stepDefinition

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.But
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable


public class EditProduct {
	//Background
	@Given("the seller is on My Listing Page")
	def the_seller_is_on_My_Listing_Page() {
		Mobile.tap(findTestObject('Object Repository/Edit Product/Akun Saya/Daftar_Jual_Saya'), 5)
	}

	//Positive case
	@When("seller click product card")
	def seller_click_product_card() {
		Mobile.tap(findTestObject('Object Repository/Edit Product/Daftar Jual Saya/Produk No1'), 60)
	}

	@When("seller change product name, price, category, description or location")
	def seller_update_the_field() {
		def device_Height = Mobile.getDeviceHeight()
		def device_Width = Mobile.getDeviceWidth()
		Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/input_namaProduk'),  ' -Edit', 5)

		Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/Input_hargaProduk'), '999000', 5)

		Mobile.tap(findTestObject('Page_Add Product/select_category'), 0)
		Mobile.setText(findTestObject('Page_Add Product/select_category'), 'Sepatu Pria', 0)
		Mobile.tap(findTestObject('Page_Add Product/category_selector'), 0)

		//		Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/Pilih Kategori'), 5)
		//		Mobile.delay(2)
		//		Mobile.tapAtPosition((device_Width*0.271), (device_Height*0.48))

		Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/input_lokasi'), ' -Edit', 5)

		Mobile.setText(findTestObject('Object Repository/Edit Product/Ubah Produk/Deskripsi'), ' -Edit', 5)

	}

	@When("tap button save changes")
	def tap_button_save_change() {
		Mobile.tap(findTestObject('Object Repository/Edit Product/Ubah Produk/button_Perbarui Produk'), 5)
	}

	@Then("the product field is successfully updated")
	def product_successfully_updated() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Edit Product/Ubah Produk/Pop_up_messages'), 5)
		Mobile.verifyMatch('Produk berhasil diperbarui', Mobile.getText(findTestObject('Object Repository/Edit Product/Ubah Produk/Pop_up_messages'), 3), false)
	}

}
