package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Transaction {
	@When("User tap transaction button")
	public void user_tap_transaction_button() {
		Mobile.tap(findTestObject('Page_Transaksi/Btn_Transaksi'), 0)
	}

	@Then("User can see text must login first")
	public void User_can_see_text_must_login_first() {
		Mobile.verifyElementText(findTestObject('Page_Transaksi/Popup Login'), 'Masuk untuk akses mudah melihat pesanan Anda di sini')
		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
	}

	@Given("User login as Buyer")
	public void user_login_as_Buyer() {
		WebUI.callTestCase(findTestCase('Step definition/Precondition/Precondition - Login Buyer'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap Transaction button")
	public void user_tap_Transaction_button() {
		Mobile.tap(findTestObject('Page_Transaksi/Btn_Transaksi'), 0)
		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
	}
	@Then("User Can see Transaction Success")
	public void user_Can_see_Transaction_Success() {
		Mobile.verifyElementText(findTestObject('Page_Transaksi/Status_Selesai'), 'Selesai', FailureHandling.STOP_ON_FAILURE)

		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User Can see Transaction Canceled")
	public void user_Can_see_Transaction_Canceled() {
		Mobile.verifyElementText(findTestObject('Page_Transaksi/Status_Dibatalkan'), 'Dibatalkan', FailureHandling.STOP_ON_FAILURE)

		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
	}
}