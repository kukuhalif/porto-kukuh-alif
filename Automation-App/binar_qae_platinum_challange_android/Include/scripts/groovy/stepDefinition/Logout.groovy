package stepDefinition

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.But
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable


public class Logout {
	//Background
	@Given("the user is logged in")
	def the_seller_is_on_My_Listing_Page() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', false)
		Mobile.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login Seller'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click log out button")
	def user_click_log_out_button() {
		WebUI.callTestCase(findTestCase('Pages/Home/Tap Profile button'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Logout/Tap Keluar button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user account will be logged out, and redirected to main page")
	def logout_success() {
		WebUI.callTestCase(findTestCase('Pages/Logout/Verification logout'), [:], FailureHandling.STOP_ON_FAILURE)
	}

}
