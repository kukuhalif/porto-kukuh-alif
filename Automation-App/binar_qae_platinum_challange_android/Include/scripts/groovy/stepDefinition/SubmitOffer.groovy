package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SubmitOffer {
	@Given("the user is logged in as a buyer")
	public void the_user_is_logged_in_as_a_buyer() {
		WebUI.callTestCase(findTestCase('Step definition/Precondition/Precondition - Login Buyer'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user can tap one product want to buy")
	public void user_can_tap_one_product_want_to_buy() {
		Mobile.delay(120, FailureHandling.STOP_ON_FAILURE)
		Mobile.tap(findTestObject('Page_Home/btn_product1'), 0)
	}

	@And("user can tap {string}")
	public void user_can_tap(String string) {
		WebUI.callTestCase(findTestCase('Pages/Submit Offer/Tap Submit Offer'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user can set the amount of the price it want to bid")
	public void user_can_set_the_amount_of_the_price_it_want_to_bid() {
		Mobile.setText(findTestObject('Page_Submit Offer/textfield_price'), '32123', 0)
	}

	@Then("tap {string} and verify the product already success")
	public void tap_and_verify_the_product_already_success(String string) {
		WebUI.callTestCase(findTestCase('Pages/Submit Offer/Tap Send Product'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Submit Offer/Verify Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}