package stepDefinition

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import cucumber.api.java.en.But
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class AddProduct {
	//Background
	@Given("the user is logged in as a seller")
	def the_seller_is_logged_in() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', false)
		Mobile.callTestCase(findTestCase('Step Definition/Precondition/Precondition - Login Seller'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	//Positive case
	@Given("the seller is on Add Product Page")
	def the_seller_is_on_Add_Product_Page() {
		Mobile.delay(2)
		Mobile.callTestCase(findTestCase('Pages/Home/Tap Add Product button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("the seller inputs {string}, {string}, {string}, {string}, and {string}")
	def the_seller_inputs(String product_name, String product_price, String category, String location, String description) {
		GlobalVariable.expected_name = product_name
		GlobalVariable.expected_price = product_price
		GlobalVariable.expected_category = category
		
		Mobile.callTestCase(findTestCase('Pages/Add Product/Input mandatory fields'), [
			('input_name') : product_name,
			('input_price') : product_price,
			('input_category') : category,
			('input_location') : location,
			('input_description') : description
		], FailureHandling.STOP_ON_FAILURE)
	}

	@When("the seller uploads a photo from camera")
	def the_seller_uploads_a_photo_from_camera() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Upload product image from real device camera'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("the seller taps publish button")
	def the_seller_clicks_submit_button() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Tap publish button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("the product is added to my listings")
	def the_page_navigates_to_the_product_page() {
		Mobile.delay(50)
		WebUI.callTestCase(findTestCase('Pages/My Listings/Verify product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

}
