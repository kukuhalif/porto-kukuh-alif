package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Catalog {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("User click Komputer dan Aksesoris {string} category button")
	public void user_click_Komputer_dan_Aksesoris_category_button(String Komputer_dan_Aksesori) {
		Mobile.delay(60, FailureHandling.STOP_ON_FAILURE)

		Mobile.tap(findTestObject('Catalog/Komputer dan Aksesoris Category'), 0)

		Mobile.delay(60, FailureHandling.STOP_ON_FAILURE)

		Komputer_dan_Aksesori = Mobile.getText(findTestObject('Catalog/Komputer dan Aksesoris_Text'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User can see item on sale in Komputer dan Aksesoris {string} category")
	public void user_can_see_item_on_sale_in_Komputer_dan_Aksesoris_category(String Komputer_dan_Aksesori) {
		Mobile.verifyMatch('Komputer dan Aksesoris', 'Komputer dan Aksesoris', false)
	}

	@When("User click  Elektronik {string} category button")
	public void user_click_Elektronik_category_button(String Elektronik) {
		Mobile.tap(findTestObject('Catalog/Elektronik Category'), 0)

		Mobile.delay(60, FailureHandling.STOP_ON_FAILURE)

		Elektronik = Mobile.getText(findTestObject('Catalog/Elektronik_Text'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User can see item on sale in Elektronik {string} category")
	public void user_can_see_item_on_sale_in_Elektronik_category(String string) {
		Mobile.verifyMatch('Elektronik', 'Elektronik', false)

		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
	}
}