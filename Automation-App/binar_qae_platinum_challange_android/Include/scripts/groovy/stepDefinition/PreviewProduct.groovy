package stepDefinition

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.But
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable


public class PreviewProduct {
	//Background
	@Given("the seller in add product page")
	def the_seller_in_add_product_page() {
		WebUI.callTestCase(findTestCase('Pages/Home/Tap Add Product button'), [:], FailureHandling.STOP_ON_FAILURE)
		
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input mandatory fields'), [('product_name') : 'Book', ('product_price') : '100000'
		        , ('category') : 'Hobi dan Koleksi', ('location') : 'Jakarta', ('description') : 'New book, 100 pages'], FailureHandling.STOP_ON_FAILURE)
		
		WebUI.callTestCase(findTestCase('Test Cases/Pages/Add Product/Upload product image from real device camera - Samsung'), 
		    [:], FailureHandling.STOP_ON_FAILURE)
	}

	//Positive case
	@When("seller click preview button")
	def seller_click_preview_button() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Tap preview button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("the preview page is displayed the new product")
	def preview_page_displayed() {
		WebUI.callTestCase(findTestCase('Pages/Preview Product/Verification'), [:], FailureHandling.STOP_ON_FAILURE)
	}

}
