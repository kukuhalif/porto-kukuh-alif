package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Profile {
	@Given("the user is logged in as a user")
	public void the_user_is_logged_in_as_a_user() {
		WebUI.callTestCase(findTestCase('Step definition/Precondition/Precondition - Login User'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user can tap account menu on bottom navbar")
	public void user_can_tap_account_menu_on_bottom_navbar() {
		WebUI.callTestCase(findTestCase('Pages/Home/Tap Profile button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("tap the edit button")
	public void tap_the_edit_buttom_pencil_icon() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Tap edit button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("verify that it is an information account")
	public void verify_that_it_is_an_information_account() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Verify profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user tap profile button")
	public void user_tap_profile_button() {
		WebUI.callTestCase(findTestCase('Pages/Home/Tap Profile button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user cant view account info")
	public void user_cant_view_account_info() {
		Mobile.verifyElementVisible(findTestObject('Temp_Helper/btn_login'), 0)
	}

	@Then("user can tap edit name account")
	public void user_can_tap_edit_name_account() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Tap edit name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user can remove previous")
	public void user_can_remove_previous_name() {
		Mobile.tap(findTestObject('Page_Profile/Change_Profile/btn_delete'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user can write new name account")
	public void user_can_write_new_name_account() {
		Mobile.setText(findTestObject('Page_Profile/Change_Profile/text_edit_name'), 'lagi testing1', 0)
	}

	@And("user save the name and verify name edit already success")
	public void user_save_the_name_and_verify_name_edit_already_success() {
		Mobile.tap(findTestObject('Page_Profile/Change_Profile/btn_simpan'), 0)
		WebUI.callTestCase(findTestCase('Pages/Profile/Verify update profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user can tap edit Nomor HP account")
	public void user_can_tap_edit_Nomor_HP_account() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Tap edit NoHp'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user can write new Nomor HP account")
	public void user_can_write_new_Nomor_HP_account() {
		Mobile.setText(findTestObject('Page_Profile/Change_Profile/text_edit_nohp'), '09123131', 0)
	}

	@Then("user can tap edit kota")
	public void user_can_tap_edit_kota() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Tap edit kota'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user can write new kota")
	public void user_can_write_new_kota() {
		Mobile.setText(findTestObject('Page_Profile/Change_Profile/text_edit_kota'), 'Bandung', 0)
	}

	@Then("user can tap edit alamat")
	public void user_can_tap_edit_alamat() {
		WebUI.callTestCase(findTestCase('Pages/Profile/Tap edit alamat'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user can write new alamat")
	public void user_can_write_new_alamat() {
		Mobile.setText(findTestObject('Page_Profile/Change_Profile/text_edit_alamat'), 'Jalan Kebon Jeruk, Kelapa Dua', 0)
	}

	@And("user save blank name and verify update profile failed and error message")
	public void user_save_blank_name_and_verify_update_profile_failed_and_error_message() {
		Mobile.tap(findTestObject('Page_Profile/Change_Profile/btn_simpan'), 0)
		WebUI.callTestCase(findTestCase('Pages/Profile/Verify error'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}