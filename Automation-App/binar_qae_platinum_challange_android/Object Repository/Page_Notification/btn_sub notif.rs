<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>

   <name>FotoProduk</name>

   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>

      <value>android.widget.ImageView</value>
      <webElementGuid>289adc4a-e594-4c1c-a655-78cf4df83a9f</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>

      <value>12</value>
      <webElementGuid>eccb6931-9818-4e5c-a477-818cb538b380</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand:id/iv_product_image</value>
      <webElementGuid>9b195499-c701-4734-92e9-fc8c11eb3960</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand</value>

      <webElementGuid>bb149ac5-4031-450b-91ca-604ea95c7706</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>

      <webElementGuid>335ef8a7-8663-4fb5-9e47-804b13b33f69</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>

      <webElementGuid>0bf31262-1f33-4301-802a-6b5cd9a9d6ae</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>true</value>

      <webElementGuid>824ec0cb-6310-49f6-9a14-146750dadc72</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>

      <webElementGuid>7a0032c4-d4cf-4cbb-ad7d-38aac1393079</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>true</value>

      <webElementGuid>720b5468-4098-4959-b798-7dc4e11830ca</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>

      <webElementGuid>21e1dab6-45e6-4e00-b988-bbc6c42025dc</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>

      <webElementGuid>e6055e4f-bfae-4752-b524-b3ef3ea461a6</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>false</value>

      <webElementGuid>f0caf6d2-4fa9-4bd7-9ec5-b39161c6f9ee</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>

      <webElementGuid>dd667bdf-5c47-41f9-8489-d0af18ebbec5</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>

      <webElementGuid>ab8b9e61-71c3-49f2-acdc-0ff1945eaa17</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>

      <value>41</value>
      <webElementGuid>4bb14e9f-bffc-4d00-bc66-597889cf3c6a</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>

      <value>1724</value>
      <webElementGuid>fccf9ae1-12a9-475a-a0d3-1ecb99e6d956</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>

      <value>245</value>
      <webElementGuid>9b330928-5012-4ee3-acc9-ba365ee3c3f1</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>

      <value>245</value>
      <webElementGuid>223dedf0-6d4c-44b6-84c8-b98f9152459e</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>

      <value>[41,1724][286,1969]</value>
      <webElementGuid>90dbfacd-d9d2-4c6f-9b64-948e23005f4b</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>

      <webElementGuid>854288c8-31b3-47ef-a339-eec7984005af</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value></value>

      <webElementGuid>d0b064e2-55cb-40ba-b86d-3976e0e8ce45</webElementGuid>

   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>

      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[2]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.widget.ImageView[1]</value>
      <webElementGuid>e43545e4-0313-47a9-8755-a6863e6726ec</webElementGuid>
   </webElementProperties>
   <locator>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[2]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.widget.ImageView[1]</locator>

   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
