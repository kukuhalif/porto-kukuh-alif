/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 58.77777777777778, "KoPercent": 41.22222222222222};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5488888888888889, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "PUT/buyer/order/{id}"], "isController": false}, {"data": [0.98, 500, 1500, "GET/buyer/order/{id}"], "isController": false}, {"data": [0.9466666666666667, 500, 1500, "GET/seller/product"], "isController": false}, {"data": [0.0, 500, 1500, "POST/buyer/order"], "isController": false}, {"data": [0.013333333333333334, 500, 1500, "POST/auth/register"], "isController": false}, {"data": [1.0, 500, 1500, "GET/buyer/product/{id}"], "isController": false}, {"data": [0.96, 500, 1500, "GET/seller/product/{id}"], "isController": false}, {"data": [0.8266666666666667, 500, 1500, "GET/buyer/product"], "isController": false}, {"data": [0.8933333333333333, 500, 1500, "POST/auth/login"], "isController": false}, {"data": [0.013333333333333334, 500, 1500, "DELETE/seller/product/{id}"], "isController": false}, {"data": [0.0, 500, 1500, "POST/seller/product"], "isController": false}, {"data": [0.9533333333333334, 500, 1500, "GET/buyer/order"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 900, 371, 41.22222222222222, 414.55111111111125, 306, 2532, 369.0, 520.9, 619.6499999999995, 1319.7500000000002, 34.39972480220158, 36.51204557724649, 49.42272961816305], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["PUT/buyer/order/{id}", 75, 75, 100.0, 364.2800000000001, 314, 679, 342.0, 446.8, 482.40000000000015, 679.0, 3.5719388484069152, 1.1790188776968138, 1.3883121695956566], "isController": false}, {"data": ["GET/buyer/order/{id}", 75, 0, 0.0, 372.9066666666667, 317, 847, 350.0, 442.4000000000002, 519.4000000000004, 847.0, 3.57347055460263, 0.9701414200971984, 1.5354756289308176], "isController": false}, {"data": ["GET/seller/product", 75, 0, 0.0, 409.65333333333314, 332, 642, 392.0, 506.20000000000005, 531.0000000000001, 642.0, 3.544590954203885, 6.311079579020748, 1.201145567489012], "isController": false}, {"data": ["POST/buyer/order", 75, 75, 100.0, 415.21333333333325, 325, 924, 357.0, 631.8000000000001, 757.0000000000003, 924.0, 3.5744924220760654, 1.2042967632971118, 1.4521375464684014], "isController": false}, {"data": ["POST/auth/register", 75, 72, 96.0, 596.8533333333334, 314, 2532, 380.0, 1379.4, 1437.6000000000001, 2532.0, 3.3533041223285345, 1.1579378297415721, 1.8436623250693016], "isController": false}, {"data": ["GET/buyer/product/{id}", 75, 0, 0.0, 332.2000000000001, 306, 462, 322.0, 371.80000000000007, 414.4000000000001, 462.0, 3.5773908895778677, 3.116242845218221, 1.229728118292392], "isController": false}, {"data": ["GET/seller/product/{id}", 75, 0, 0.0, 389.1600000000001, 322, 759, 370.0, 461.20000000000005, 505.6, 759.0, 3.549455750118315, 2.890865327733081, 1.2235916794841457], "isController": false}, {"data": ["GET/buyer/product", 75, 0, 0.0, 482.7866666666667, 357, 858, 454.0, 621.4000000000001, 789.2, 858.0, 3.556525037936267, 16.7962451986912, 1.4101066068380121], "isController": false}, {"data": ["POST/auth/login", 75, 0, 0.0, 455.17333333333323, 381, 643, 444.0, 539.6000000000001, 587.6, 643.0, 3.5155151401518703, 1.7405919688056624, 1.4899741902596795], "isController": false}, {"data": ["DELETE/seller/product/{id}", 75, 74, 98.66666666666667, 379.8, 319, 589, 366.0, 453.8000000000002, 523.0, 589.0, 3.550127804600965, 1.1847627035406607, 1.3000956315677363], "isController": false}, {"data": ["POST/seller/product", 75, 75, 100.0, 377.53333333333336, 319, 672, 351.0, 456.40000000000003, 526.4000000000001, 672.0, 3.520465640255351, 1.1998461996573415, 45.47176439870447], "isController": false}, {"data": ["GET/buyer/order", 75, 0, 0.0, 399.0533333333334, 323, 843, 362.0, 516.0000000000005, 670.4000000000001, 843.0, 3.5719388484069152, 7.482235185383626, 1.199948206886698], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 222, 59.83827493261455, 24.666666666666668], "isController": false}, {"data": ["404/Not Found", 149, 40.16172506738545, 16.555555555555557], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 900, 371, "400/Bad Request", 222, "404/Not Found", 149, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["PUT/buyer/order/{id}", 75, 75, "404/Not Found", 75, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["POST/buyer/order", 75, 75, "400/Bad Request", 75, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST/auth/register", 75, 72, "400/Bad Request", 72, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["DELETE/seller/product/{id}", 75, 74, "404/Not Found", 74, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST/seller/product", 75, 75, "400/Bad Request", 75, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
