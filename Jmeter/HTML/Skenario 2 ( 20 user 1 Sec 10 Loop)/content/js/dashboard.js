/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 66.83333333333333, "KoPercent": 33.166666666666664};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5989583333333334, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9575, 500, 1500, "PUT/buyer/order/{id}"], "isController": false}, {"data": [0.9875, 500, 1500, "GET/buyer/order/{id}"], "isController": false}, {"data": [0.85, 500, 1500, "GET/seller/product"], "isController": false}, {"data": [0.0, 500, 1500, "POST/buyer/order"], "isController": false}, {"data": [0.0, 500, 1500, "POST/auth/register"], "isController": false}, {"data": [0.97, 500, 1500, "GET/buyer/product/{id}"], "isController": false}, {"data": [0.9025, 500, 1500, "GET/seller/product/{id}"], "isController": false}, {"data": [0.715, 500, 1500, "GET/buyer/product"], "isController": false}, {"data": [0.83, 500, 1500, "POST/auth/login"], "isController": false}, {"data": [0.0025, 500, 1500, "DELETE/seller/product/{id}"], "isController": false}, {"data": [0.0, 500, 1500, "POST/seller/product"], "isController": false}, {"data": [0.9725, 500, 1500, "GET/buyer/order"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2400, 796, 33.166666666666664, 442.6320833333332, 306, 1983, 409.0, 560.0, 638.9499999999998, 1099.0, 43.4286955105586, 48.7632717641098, 62.38209787742251], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["PUT/buyer/order/{id}", 200, 0, 0.0, 407.53000000000014, 330, 1004, 386.0, 482.70000000000005, 521.5999999999999, 766.1100000000008, 4.074149521287431, 2.689575269912406, 1.5835073334691383], "isController": false}, {"data": ["GET/buyer/order/{id}", 200, 0, 0.0, 382.99000000000024, 326, 578, 369.5, 440.9, 467.9, 512.97, 4.068431009581155, 4.783584897984093, 1.7481539494294025], "isController": false}, {"data": ["GET/seller/product", 200, 0, 0.0, 461.48000000000013, 337, 769, 446.5, 575.7, 622.6999999999999, 724.9300000000001, 4.0127605786400755, 5.211553207951285, 1.3597928913946349], "isController": false}, {"data": ["POST/buyer/order", 200, 200, 100.0, 416.82499999999976, 331, 966, 395.5, 523.9000000000001, 587.75, 760.98, 4.058112166220274, 1.367235056001948, 1.6486080675269865], "isController": false}, {"data": ["POST/auth/register", 200, 197, 98.5, 517.2249999999999, 318, 1983, 403.0, 1257.500000000004, 1481.2999999999993, 1801.9, 3.9003081243418234, 1.3239146478509303, 2.1405987948047898], "isController": false}, {"data": ["GET/buyer/product/{id}", 200, 0, 0.0, 351.75999999999976, 306, 609, 331.0, 417.8, 504.95, 594.94, 4.060171745264825, 3.497140116526929, 1.3956840374347834], "isController": false}, {"data": ["GET/seller/product/{id}", 200, 0, 0.0, 490.0100000000001, 332, 1269, 414.5, 959.9, 1033.9, 1264.880000000001, 4.017516371379213, 3.272078763408461, 1.3849446084930297], "isController": false}, {"data": ["GET/buyer/product", 200, 0, 0.0, 532.5000000000001, 352, 963, 513.0, 703.3000000000001, 761.4499999999998, 952.9000000000001, 4.042690815006468, 19.09223904430789, 1.6028637411060802], "isController": false}, {"data": ["POST/auth/login", 200, 0, 0.0, 495.68000000000006, 395, 878, 479.5, 617.4000000000001, 668.8499999999999, 773.6400000000003, 3.988592625092236, 1.9748207626189098, 1.6904777336816703], "isController": false}, {"data": ["DELETE/seller/product/{id}", 200, 199, 99.5, 429.8999999999999, 325, 736, 417.0, 525.6, 641.1499999999996, 733.98, 4.02844079199146, 1.3450428777166799, 1.475259079098435], "isController": false}, {"data": ["POST/seller/product", 200, 200, 100.0, 424.00499999999977, 328, 904, 407.5, 521.9, 568.6999999999999, 724.99, 3.9928129367139147, 1.3608317528448792, 51.56265596925534], "isController": false}, {"data": ["GET/buyer/order", 200, 0, 0.0, 401.6800000000002, 323, 708, 390.5, 478.9, 509.6499999999999, 600.99, 4.06454497419014, 8.51411032191196, 1.3654330772669998], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 597, 75.0, 24.875], "isController": false}, {"data": ["404/Not Found", 199, 25.0, 8.291666666666666], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2400, 796, "400/Bad Request", 597, "404/Not Found", 199, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["POST/buyer/order", 200, 200, "400/Bad Request", 200, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST/auth/register", 200, 197, "400/Bad Request", 197, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["DELETE/seller/product/{id}", 200, 199, "404/Not Found", 199, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST/seller/product", 200, 200, "400/Bad Request", 200, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
