/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 64.73429951690821, "KoPercent": 35.265700483091784};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5862016908212561, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.697463768115942, 500, 1500, "PUT/buyer/order/{id}"], "isController": false}, {"data": [0.9855072463768116, 500, 1500, "GET/buyer/order/{id}"], "isController": false}, {"data": [0.8768115942028986, 500, 1500, "GET/seller/product"], "isController": false}, {"data": [0.0036231884057971015, 500, 1500, "POST/buyer/order"], "isController": false}, {"data": [0.0036231884057971015, 500, 1500, "POST/auth/register"], "isController": false}, {"data": [0.9782608695652174, 500, 1500, "GET/buyer/product/{id}"], "isController": false}, {"data": [0.9184782608695652, 500, 1500, "GET/seller/product/{id}"], "isController": false}, {"data": [0.7463768115942029, 500, 1500, "GET/buyer/product"], "isController": false}, {"data": [0.8478260869565217, 500, 1500, "POST/auth/login"], "isController": false}, {"data": [0.009057971014492754, 500, 1500, "DELETE/seller/product/{id}"], "isController": false}, {"data": [0.0, 500, 1500, "POST/seller/product"], "isController": false}, {"data": [0.967391304347826, 500, 1500, "GET/buyer/order"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3312, 1168, 35.265700483091784, 435.0202294685997, 306, 2532, 398.0, 551.7000000000003, 633.3499999999999, 1281.6099999999997, 5.123714817003555, 5.66901053188708, 7.360218582632278], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["PUT/buyer/order/{id}", 276, 75, 27.17391304347826, 395.52173913043475, 314, 1004, 373.0, 475.0, 515.1999999999998, 699.2400000000016, 0.430334195041178, 0.24548892142814824, 0.16725879846327035], "isController": false}, {"data": ["GET/buyer/order/{id}", 276, 0, 0.0, 380.04710144927515, 317, 847, 363.5, 440.3, 480.15, 640.0400000000009, 0.4303328531068161, 0.4002303830196269, 0.18490864781933503], "isController": false}, {"data": ["GET/seller/product", 276, 0, 0.0, 447.03623188405794, 332, 769, 426.0, 565.9000000000001, 612.3, 719.6100000000001, 0.43030132005843363, 0.615800859842597, 0.14581499810573878], "isController": false}, {"data": ["POST/buyer/order", 276, 275, 99.6376811594203, 416.11231884057986, 325, 966, 384.0, 556.5, 630.0499999999998, 923.23, 0.4303241307374602, 0.14549993607503856, 0.17481917811209322], "isController": false}, {"data": ["POST/auth/register", 276, 269, 97.46376811594203, 542.7210144927535, 314, 2532, 397.0, 1336.5000000000002, 1480.75, 1843.6300000000033, 0.42949911999365087, 0.14683033102166324, 0.2358336533179585], "isController": false}, {"data": ["GET/buyer/product/{id}", 276, 0, 0.0, 346.28985507246375, 306, 609, 328.0, 403.6, 483.29999999999995, 590.3800000000001, 0.4303529830010572, 0.3718171566999411, 0.1479338379066134], "isController": false}, {"data": ["GET/seller/product/{id}", 276, 0, 0.0, 462.0579710144929, 322, 1269, 402.5, 583.8000000000001, 1010.3, 1179.760000000002, 0.43032010514778224, 0.3504755543879398, 0.1483427706222335], "isController": false}, {"data": ["GET/buyer/product", 276, 0, 0.0, 518.3768115942032, 352, 963, 501.5, 679.8000000000001, 765.7499999999999, 945.3000000000002, 0.4303221179332065, 2.032263439770495, 0.1706159959774237], "isController": false}, {"data": ["POST/auth/login", 276, 0, 0.0, 484.31884057970984, 381, 878, 468.5, 596.0, 654.8999999999999, 746.2800000000007, 0.4302751578455063, 0.2130366260035856, 0.18236271338373994], "isController": false}, {"data": ["DELETE/seller/product/{id}", 276, 273, 98.91304347826087, 416.0000000000001, 319, 736, 395.5, 514.2, 584.7499999999999, 732.46, 0.4303261435605435, 0.1436308515000608, 0.1575901404640662], "isController": false}, {"data": ["POST/seller/product", 276, 276, 100.0, 411.05072463768107, 319, 904, 394.0, 518.0, 559.5999999999999, 724.23, 0.4303046744183871, 0.14665657360548545, 5.55717025370826], "isController": false}, {"data": ["GET/buyer/order", 276, 0, 0.0, 400.71014492753625, 323, 843, 379.0, 481.4000000000001, 523.6499999999993, 728.0100000000016, 0.4303254726174235, 0.9014141980120834, 0.14456246345741572], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 820, 70.20547945205479, 24.758454106280194], "isController": false}, {"data": ["404/Not Found", 348, 29.794520547945204, 10.507246376811594], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3312, 1168, "400/Bad Request", 820, "404/Not Found", 348, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["PUT/buyer/order/{id}", 276, 75, "404/Not Found", 75, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["POST/buyer/order", 276, 275, "400/Bad Request", 275, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST/auth/register", 276, 269, "400/Bad Request", 269, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["DELETE/seller/product/{id}", 276, 273, "404/Not Found", 273, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST/seller/product", 276, 276, "400/Bad Request", 276, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
